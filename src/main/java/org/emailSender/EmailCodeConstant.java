package org.emailSender;

public class EmailCodeConstant {
    public static final String SMTP_HOST = "smtpHost";
    public static final String SMTP_PASSWORD = "smtpPassword";
    public static final String SMTP_USER = "smtpUser";
    public static final String SMTP_AUTH_REQ = "smtpAuthReq";
    public static final String SMTP_PORT = "smtpPort";
    public static final String EMAIL_BODY = "body";
    public static final String CC_EMAIL = "ccEmail";
    public static final String SENDER_EMAIL = "senderEmail";
    public static final String RECEIVER_EMAIL = "receiverEmail";
    public static final String ATTACHMENT_PATH = "attachmentPath";
    public static final String SUBJECT = "subject";
}
