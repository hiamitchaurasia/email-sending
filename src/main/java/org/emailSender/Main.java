package org.emailSender;
import org.apache.commons.cli.*;

import java.util.Properties;

public class Main {
    public static void main(String[] args) throws Exception {
//        if(args.length < 2) {
//            System.out.println("No command line arguments passed!");
//            System.out.println("Command Format: java -jar xyz.jar <sender@xyz.com> " +
//                    "<receiver1@xyz.com,receiver2@xyz.com,receiver3@xyz.com> " +
//                    "<cc1@xyz.com,cc2@xyz.com,cc3@xyz.com> <SUBJECT> <EMAIL_BODY_HTML_FILE> " +
//                    "<ATTACHMENT> <HOST> <USER> <PASSWORD>");
//        }
//        // Print all command line arguments
//        for(String arg: args) {
//            System.out.println(arg);
//        }
//        String senderEmail = args[0];
//        String emailToList = args[1];
//        // Access individual arguments
//        System.out.println("First argument: " + args[0]);

        Options options = setCliOptions();

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("java -jar xyz.jar", options);
            System.exit(1);
        }

        String senderEmail = cmd.getOptionValue("senderEmail");
        String receiverEmail = cmd.getOptionValue("receiverEmail");
        String attachmentPath = cmd.getOptionValue("attachmentPath");
        String body = cmd.getOptionValue("body");
        String smtpHost = cmd.getOptionValue("smtpHost");
        String ccEmail = cmd.getOptionValue("ccEmail");
        String smtpAuthReq = cmd.getOptionValue("smtpAuthReq");
        String subject = cmd.getOptionValue("subject");
        String smtpUser = cmd.getOptionValue("smtpUser");
        String smtpPassword = cmd.getOptionValue("smtpPassword");
        String smtpPort = cmd.getOptionValue("smtpPort");
//        if(smtpPassword == null){
//            smtpPassword = "";
//        }
        if(smtpAuthReq == null || !(smtpAuthReq.equals("true") || smtpAuthReq.equals("false"))){
            smtpAuthReq = "false";
        }
        if(smtpPort == null){
            smtpPort = "25";
        }
        Properties props = new Properties();
        props.put(EmailCodeConstant.SMTP_AUTH_REQ,smtpAuthReq);
        props.put(EmailCodeConstant.SMTP_HOST,smtpHost);
        props.put(EmailCodeConstant.SMTP_PORT,smtpPort);
        props.put(EmailCodeConstant.SMTP_PASSWORD,ifNullReturnEmpty(smtpPassword));
        props.put(EmailCodeConstant.SMTP_USER,smtpUser);

        System.out.println("Starting email sending!");
        SendEmail sendEmail = new SendEmail(props);
        Properties emailProps = new Properties();
        emailProps.put(EmailCodeConstant.EMAIL_BODY,body);
        emailProps.put(EmailCodeConstant.CC_EMAIL,ifNullReturnEmpty(ccEmail));
        emailProps.put(EmailCodeConstant.SENDER_EMAIL,senderEmail);
        emailProps.put(EmailCodeConstant.RECEIVER_EMAIL,receiverEmail);
        emailProps.put(EmailCodeConstant.ATTACHMENT_PATH,ifNullReturnEmpty(attachmentPath));
        emailProps.put(EmailCodeConstant.SUBJECT,subject);
        sendEmail.sendEmail(emailProps);
    }

    private static Options setCliOptions(){
        Options options = new Options();
        Option senderEmail = new Option("s", "senderEmail", true, "Sender Email Address");
        senderEmail.setRequired(true);
        options.addOption(senderEmail);

        Option receiverEmail = new Option("r", "receiverEmail", true, "Receiver Email Address (comma separated)");
        receiverEmail.setRequired(true);
        options.addOption(receiverEmail);

        Option ccEmail = new Option("o", "ccEmail", true, "CC Email Address (comma separated)");
        ccEmail.setRequired(false);
        options.addOption(ccEmail);

        Option emailBodyHtmlFileLocation = new Option("b", "body", true, "Html File Absolute Location for Email Body");
        emailBodyHtmlFileLocation.setRequired(true);
        options.addOption(emailBodyHtmlFileLocation);

        Option attachmentPath = new Option("a", "attachmentPath", true, "Attachment Absolute Path (comma separated)");
        attachmentPath.setRequired(false);
        options.addOption(attachmentPath);

        Option subject = new Option("sbj", "subject", true, "Subject");
        subject.setRequired(true);
        options.addOption(subject);

        Option smtpHost = new Option("h", "smtpHost", true, "SMTP Host");
        smtpHost.setRequired(true);
        options.addOption(smtpHost);

        Option smtpUser = new Option("u", "smtpUser", true, "SMTP User");
        smtpUser.setRequired(true);
        options.addOption(smtpUser);

        Option smtpPassword = new Option("pass", "smtpPassword", true, "SMTP Password");
        smtpPassword.setRequired(false);
        options.addOption(smtpPassword);

        Option smtpAuthReq = new Option("auth", "smtpAuthReq", true, "SMTP Authentication Required (true/false)");
        smtpAuthReq.setRequired(false);
        options.addOption(smtpAuthReq);

        Option smtpPort = new Option("pass", "smtpPort", true, "SMTP Port");
        smtpPort.setRequired(false);
        options.addOption(smtpPort);

        return options;
    }

    private static String ifNullReturnEmpty(String value){
        if(value == null){
            return "";
        }
        return value;
    }
}