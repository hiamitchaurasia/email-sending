package org.emailSender;

import jakarta.mail.*;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;
import java.util.StringTokenizer;

public class SendEmail {
    private final Properties prop;
    private final String username;
    private final String password;
    public SendEmail(Properties props){
        this.username = props.getProperty(EmailCodeConstant.SMTP_USER);
        this.password = props.getProperty(EmailCodeConstant.SMTP_PASSWORD);
        prop = new Properties();
        prop.put("mail.smtp.auth", Boolean.valueOf(props.getProperty(EmailCodeConstant.SMTP_AUTH_REQ)));
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", props.getProperty(EmailCodeConstant.SMTP_HOST));
        prop.put("mail.smtp.port", props.getProperty(EmailCodeConstant.SMTP_PORT));
        prop.put("mail.smtp.ssl.trust", props.getProperty(EmailCodeConstant.SMTP_HOST));
    }

    public void sendEmail(Properties props) throws Exception {
        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(props.getProperty(EmailCodeConstant.SENDER_EMAIL)));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(props.getProperty(EmailCodeConstant.RECEIVER_EMAIL)));
        if(props.getProperty(EmailCodeConstant.SENDER_EMAIL) != null && !props.getProperty(EmailCodeConstant.SENDER_EMAIL).isEmpty()) {
            message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(props.getProperty(EmailCodeConstant.CC_EMAIL)));
        }
        message.setSubject(props.getProperty(EmailCodeConstant.SUBJECT));
        MimeBodyPart[] attachmentBodyPart = null;
        int count = 0;
        if(!props.getProperty(EmailCodeConstant.ATTACHMENT_PATH).isEmpty()) {
            StringTokenizer attachments = new StringTokenizer(props.getProperty(EmailCodeConstant.ATTACHMENT_PATH),",");
            attachmentBodyPart = new MimeBodyPart[attachments.countTokens()];
            while(attachments.hasMoreTokens()){
                String filePath = attachments.nextToken();
                System.out.println("Attaching File: " + filePath);
                attachmentBodyPart[count] = new MimeBodyPart();
                attachmentBodyPart[count++].attachFile(new File(filePath));
            }
        }
        //String msg = "This is my <b style='color:red;'>bold-red email</b> using JavaMailer";
        String htmlBody = readHTMLFileAsString(props.getProperty(EmailCodeConstant.EMAIL_BODY));
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(htmlBody, "text/html; charset=utf-8");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);
        if(attachmentBodyPart != null) {
            for(int i = 0; i<count; i++) {
                multipart.addBodyPart(attachmentBodyPart[i]);
            }
        }
        message.setContent(multipart);

        Transport.send(message);
        System.out.println("Sending email completed!");
    }

    // Read HTML file content as String
    private static String readHTMLFileAsString(String path) throws Exception {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(path));
        String line;
        while((line = br.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }
}
